//
//  DataUtil.swift
//  AmazingFacts
//
//  Created by pradip.deore on 07/08/20.
//  Copyright © 2020 pradip.deore. All rights reserved.
//

import UIKit

class DataUtil: NSObject {
    /**
     This class method will convert ISO-8859-1 data to utf8 data.
     
     - parameter: ISO-8859-1 data.
     - returns: utf8 data .
     
     */
    static func convertLatin1ToUtf(_ data:Data) -> Data? {
        if let utfString = String(data: data, encoding: .isoLatin1) {
            let data = utfString.data(using: .utf8)!
            return data
        }
        return nil
    }
}
