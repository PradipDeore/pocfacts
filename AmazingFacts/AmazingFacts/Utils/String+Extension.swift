//
//  String+Extension.swift
//  AmazingFacts
//
//  Created by pradip.deore on 07/08/20.
//  Copyright © 2020 pradip.deore. All rights reserved.
//

import UIKit


extension String{
    func removePercentEncoding(_ input:String)->String{
        let urlString = self.replacingOccurrences(of: "%[0-9a-fA-F]{2}",
                                                             with: "",
                                                             options: .regularExpression,
                                                             range: nil)
        return urlString
    }
    
}
