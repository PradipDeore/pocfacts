//
//  ViewController.swift
//  AmazingFacts
//
//  Created by pradip.deore on 07/08/20.
//  Copyright © 2020 pradip.deore. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private let listViewController:ListTableViewController = {
        let view = ListTableViewController()
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        loadListView()
    }
    
    func loadListView(){

        addChild(listViewController)
        listViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(listViewController.view)
        listViewController.delegate = self

        NSLayoutConstraint.activate([
            listViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            listViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            listViewController.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            listViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
            ])
        
        listViewController.didMove(toParent: self)
    }

}

extension ViewController:HeaderDelegate{
    func setHeader(_ titleString: String) {
         self.title = titleString
    }
    
    
}

