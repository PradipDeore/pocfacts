//
//  FactModel.swift
//  AmazingFacts
//
//  Created by pradip.deore on 07/08/20.
//  Copyright © 2020 pradip.deore. All rights reserved.
//

import UIKit

struct FactResponse:Codable{
    var title:String?
    var rows:[FactModel]?
}

struct FactModel:Codable {
    
    var title:String?
    var description:String?
    var imageHref:String?

}
