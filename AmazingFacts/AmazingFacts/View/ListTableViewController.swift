//
//  ListTableViewController.swift
//  AmazingFacts
//
//  Created by pradip.deore on 07/08/20.
//  Copyright © 2020 pradip.deore. All rights reserved.
//

import UIKit
import ListPlaceholder

protocol HeaderDelegate {
    func setHeader(_ titleString:String)
}


class ListTableViewController: UITableViewController {
    var delegate:HeaderDelegate?
    private let customRefreshControl = UIRefreshControl()
    var isLoading = true

     let factListViewModel:FactListViewModel = {
         let viewModel = FactListViewModel()
         return viewModel
     }()


    override func viewDidLoad() {
        super.viewDidLoad()
        isLoading = true

        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(ListTableViewCell.self, forCellReuseIdentifier: ListTableViewCell.ID)
        tableView.reloadData()
        tableView.showLoader()
        //Pull to refresh for tableview
        tableView.refreshControl = customRefreshControl
        refreshControl?.addTarget(self, action: #selector(fecthFacts), for: .valueChanged)
        
        self.fecthFacts()
    }
    
    @objc func fecthFacts(){
        factListViewModel.fetchFacts { [weak self](title, error) in
            DispatchQueue.main.async {
                
                if let error = error {
                    self?.presentAlert(withTitle: "Error", message: error)
                    return
                }
                self?.isLoading = false
                self?.tableView.hideLoader()
                self?.delegate?.setHeader(title ?? "Not found")
                self?.customRefreshControl.endRefreshing()
                self?.tableView.reloadData()
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isLoading{
            return 10
        }
        return factListViewModel.facts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListTableViewCell.ID, for: indexPath) as! ListTableViewCell
        cell.selectionStyle = .none

        if isLoading{
            return cell
        }
        cell.factViewModel = factListViewModel.facts[indexPath.row]
        return cell
    }

}
