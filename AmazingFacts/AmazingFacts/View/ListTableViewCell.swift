//
//  ListTableViewCell.swift
//  AmazingFacts
//
//  Created by pradip.deore on 07/08/20.
//  Copyright © 2020 pradip.deore. All rights reserved.
//

import UIKit
import Kingfisher

class ListTableViewCell: UITableViewCell {
    
    static let ID = "ListTableViewCell"
    let noImage = #imageLiteral(resourceName: "NoFact")
    
    
        var factViewModel : FactViewModel? {
            didSet {
                if let title = factViewModel?.title{
                    titleLabel.text = title
                }
                if let desc = factViewModel?.description{
                    descriptionLabel.text = desc
                }
                
                if let urlString = factViewModel?.imageHref, let url = URL(string: urlString){
                    iconImageView.kf.setImage(with: url, placeholder:noImage)
                }
            }
        }
        
        
        private let titleLabel : UILabel = {
            let lbl = UILabel()
            lbl.textColor = .darkGray
            lbl.font = UIFont.boldSystemFont(ofSize: 16)
            lbl.textAlignment = .left
            lbl.numberOfLines = 0
            return lbl
        }()
        
        
        private let descriptionLabel : UILabel = {
            let lbl = UILabel()
            lbl.textColor = .gray
            lbl.font = UIFont.italicSystemFont(ofSize: 16)
            lbl.textAlignment = .left
            lbl.numberOfLines = 0
            return lbl
        }()

        private let iconImageView : UIImageView = {
            let imgView = UIImageView(image: nil)
            imgView.contentMode = .scaleAspectFit
            imgView.layer.cornerRadius = 4
            imgView.clipsToBounds = true
            return imgView
        }()
    
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
        
            iconImageView.image = noImage
        
            iconImageView.translatesAutoresizingMaskIntoConstraints = false
            titleLabel.translatesAutoresizingMaskIntoConstraints = false
            descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        
            self.contentView.addSubview(iconImageView)
            self.contentView.addSubview(titleLabel)
            self.contentView.addSubview(descriptionLabel)
        
            let marginGuide = contentView.layoutMarginsGuide
        
        // configure icon image
            self.contentView.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
            self.contentView.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
            self.contentView.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
            let heightConstraintContent = self.contentView.heightAnchor.constraint(greaterThanOrEqualToConstant: 90)
        heightConstraintContent.isActive = true
        
            // configure icon image
        
            iconImageView.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
            iconImageView.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
            titleLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
            iconImageView.widthAnchor.constraint(equalToConstant: 90).isActive = true
            let heightConstraintImage = iconImageView.heightAnchor.constraint(equalToConstant: 70)
            heightConstraintImage.isActive = true

        
            // configure titleLabel
            titleLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant:10).isActive = true
            titleLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
            titleLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        
            descriptionLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant:10).isActive = true
            descriptionLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,constant:5).isActive = true
            descriptionLabel.textColor = UIColor.gray
            let bottomConstraintLabel = descriptionLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor,constant:-10)
            bottomConstraintLabel.isActive = true
        }

    
    override func prepareForReuse() {
        // invoke superclass implementation
        super.prepareForReuse()
        
        titleLabel.text = ""
        descriptionLabel.text = ""
        iconImageView.image = noImage
        
    }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
}
