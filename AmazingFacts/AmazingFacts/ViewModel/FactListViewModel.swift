//
//  FactListViewModel.swift
//  AmazingFacts
//
//  Created by pradip.deore on 07/08/20.
//  Copyright © 2020 pradip.deore. All rights reserved.
//

import UIKit

class FactViewModel {
    
    private (set) var title, description, imageHref: String!

    init(_ fact:FactModel) {
        self.title = fact.title
        self.description = fact.description
        if let urlString = fact.imageHref{
            self.imageHref = urlString.removePercentEncoding(urlString)
        }
    }
}


class FactListViewModel {
    
    private(set) var facts :[FactViewModel] = [FactViewModel]()
    private(set) var factResponse:FactResponse?
    
    func fetchFacts(completion:@escaping (String?,String?)->()){
        
        let apiManager = APIManager()
        apiManager.fetch { (factResponse, errorString) in
            self.factResponse = factResponse
            
            if let errorString = errorString{
                 completion(nil,errorString)
            }else if let factsList =  self.factResponse?.rows{
                self.facts = factsList.compactMap(FactViewModel.init).filter(){$0.imageHref != nil || $0.title != nil || $0.description != nil}
                completion(factResponse?.title,errorString)
            }
        }
    }
    
}
