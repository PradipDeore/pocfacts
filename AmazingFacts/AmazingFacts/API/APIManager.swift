//
//  APIManager.swift
//  AmazingFacts
//
//  Created by pradip.deore on 07/08/20.
//  Copyright © 2020 pradip.deore. All rights reserved.
//

import Foundation

class APIManager{
    
    let url = URL(string: "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")!
    
    /**
     This method will fetch data from server.
     
     - parameter: completion block.
     - returns: .
     
     */
    
    func fetch(completion:@escaping(FactResponse? , String?)->()){
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { data, response, error in
            
            if error != nil  {
                completion(nil, error?.localizedDescription)
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                completion(nil,"Server error!")
                return
            }
            
            guard let data = data else {
                completion(nil, "Client error!")
                return
            }
            
            guard let utfData = DataUtil.convertLatin1ToUtf(data) else {
                completion(nil, "Data error!")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let  factData = try decoder.decode(FactResponse.self, from: utfData)
                completion(factData, nil)
                
            } catch let err {
                completion(nil, err.localizedDescription)
            }
            
        }
        
        task.resume()
    }
}


